import React from 'react';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import HomePage from './page/HomePage';
import CommentList from './page/CommentList';
import ViewPost from './page/ViewPost';
import ViewAlbum from './page/ViewAlbum';
import ViewPhoto from './page/ViewPhoto';

function App() {
    return (
            <div className="container" style={{ maxWidth: '900px' }}>
            <Routes>
                <Route path='/' element={<HomePage />} />
                <Route path='/post/:userId' element={<ViewPost />} />
                <Route path='/albums/:userId' element={<ViewAlbum />} />
                <Route path='/albums/:userId/photos/:id' element={<ViewPhoto />} />
                <Route path='/post/:userId/comments/:id' element={<CommentList />} />
            </Routes>
          </div>
    );
}
export default App;
