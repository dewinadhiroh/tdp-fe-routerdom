import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import Comment from '../components/Comment'
import Title from '../components/Title';
import PostComment from './PostComment'

const CommentList = () => {
    const [commentList, setCommentList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    let params = useParams();

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://jsonplaceholder.typicode.com/comments/?postId=${params.id}`)
            .then(function (response) {
                const { data } = response;

                setCommentList(data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    console.log("Comment list", commentList)

    return (
        <div>
            <Title />
            <PostComment />
            <div className='text-center mb-5'>
                <h5>Comment List</h5>
            </div>
            {
                isLoading ? <div>Loading . . . </div> :

                    commentList?.map(item => {
                        return <Comment key={item.id} item={item} />
                    })
            }
        </div>
    )
}
export default CommentList