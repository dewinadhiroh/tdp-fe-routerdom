import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import ItemRow from '../components/ItemRow';
import Title from '../components/Title';


const HomePage = () => {
    const [dataList, setdataList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        Axios.get('https://jsonplaceholder.typicode.com/users')
            .then(function (response) {
                const { data } = response;

                setdataList(data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    return (
            <div className="container" style={{ maxWidth: '900px' }}>
                <Title />
                {
                    isLoading ? <div>Loading . . . </div> :
                        dataList?.map(item => {
                            return <ItemRow key={item.id} item={item} />
                        })
                }
                </div>
    )
}
export default HomePage

