import React from 'react'
import Posts from './Posts'
import UserDetail from './UserDetail'

const ViewPost = () => {
    return (
        <div>
            <UserDetail />
            <Posts />
        </div>
    )
}

export default ViewPost