import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import CommentView from '../components/CommentView';

const PostComment = () => {
    const [postList, setPostList] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    let params = useParams();

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://jsonplaceholder.typicode.com/posts/${params.id}`)
            .then(function (response) {
                const { data } = response;
                setPostList(data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    console.log("Data post list", postList)
    return (
        <div className='mb-5'>
            {
                isLoading ? <div>Loading . . . </div> :
                    <CommentView key={postList.id} item={postList} />
            }
        </div>
    )
}
export default PostComment