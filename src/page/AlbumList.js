import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import Album from '../components/Album'

const AlbumList = () => {
    const [albumList, setAlbumList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    let params = useParams();

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://jsonplaceholder.typicode.com/albums/?userId=${params.userId}`)
            .then(function (response) {
                const { data } = response;

                setAlbumList(data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    console.log("Comment list", albumList)
    return (
        <div className='mb-5'>
            <div className='text-center mb-5'>
                <h5>Album List</h5>
            </div>
            <div className='row'>
                {
                    isLoading ? <div>Loading . . . </div> :
                        albumList?.map(item => {
                            return <Album key={item.id} item={item} />
                        })
                }
            </div>
        </div>
    )
}
export default AlbumList