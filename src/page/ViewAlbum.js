import React from 'react'
import AlbumList from './AlbumList'
import UserDetail from './UserDetail'

const ViewAlbum = () => {
    return (
        <div>
            <UserDetail />
            <AlbumList />
        </div>
    )
}
export default ViewAlbum