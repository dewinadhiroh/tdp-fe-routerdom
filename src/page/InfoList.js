import React from 'react'
import Post from '../components/Post'
import CommentList from './CommentList'

const InfoList = () => {
  return (
    <div>
        <Post />
        <CommentList />
    </div>
  )
}
export default InfoList