import React from 'react'
import Photos from './Photos'
import UserDetail from './UserDetail'

const ViewPhoto = () => {
    return (
        <div>
            <UserDetail />
            <Photos />
        </div>
    )
}

export default ViewPhoto