import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import Photo from '../components/Photo'

const Photos = () => {
    const [photos, setPhotos] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    let params = useParams();

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://jsonplaceholder.typicode.com/photos/?albumId=${params.userId}`)
            .then(function (response) {
                const { data } = response;

                setPhotos(data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    console.log("Photos", photos)
    return (
        <div className='mb-5'>
            <div className='text-center mb-5'>
                <h5>Photo List</h5>
            </div>
            <div className='row'>
            {
                isLoading ? <div>Loading . . . </div> :
                    photos?.map(item => {
                        return <Photo key={item.id} item={item} />
                    })
            }
            </div>
        </div>
    )
}
export default Photos