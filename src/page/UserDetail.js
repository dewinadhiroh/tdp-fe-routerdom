import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import Title from '../components/Title';
import UserInfo from '../components/UserInfo'

const UserDetail = () => {
    const [detailUser, setdetailUser] = useState([])
    const [isLoading, setisLoading] = useState(false)
    let params = useParams();

    useEffect(() => {
        setisLoading(true);
        axios.get(`https://jsonplaceholder.typicode.com/users/${params?.userId}`)
            .then(function (response) {
                const { data } = response;

                setdetailUser(data);
                setisLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    console.log("detailUser", detailUser)
    return (
        <div className='mb-5'>
             <Title />
            {
                isLoading ? <div>Loading . . . </div> :
                <UserInfo key={detailUser.id} item={detailUser} />
            } 
        </div>
    )
}
export default UserDetail

