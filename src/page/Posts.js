import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import Post from '../components/Post';

const Posts = () => {
    const [postList, setPostList] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    let params = useParams();

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://jsonplaceholder.typicode.com/posts/?userId=${params?.userId}`)
            .then(function (response) {
                const { data } = response;
                setPostList(data);
                setIsLoading(false);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    console.log("Data post list", postList)
    return (
        <div className='mb-5'>
            <div className='text-center mb-5'>
                <h5>Post List</h5>
            </div>
            {
                isLoading ? <div>Loading . . . </div> :
                    postList?.map(item2 => {
                        return <Post key={item2.id} item={item2} />
                    })
            }
        </div>
    )
}
export default Posts