import React from 'react'
import { Link } from 'react-router-dom'
import { Card, CardHeader, CardBody, Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const ItemRow = ({ item }) => {
    return (
        <div className="container" style={{ maxWidth: '900px' }}>
            <Card className='mt-3'>
                <CardHeader>{item.name}</CardHeader>
                <CardBody>
                    <div className='row'>
                        <div className="col-sm">
                            <label className="form-label">
                                <strong>Username</strong>
                            </label>
                            <div className="col-sm-12">{item.username}</div>
                        </div>
                        <div className="col-sm">
                            <label className="form-label">
                                <strong>Email</strong>
                            </label>
                            <div className="col-sm-12">{item.email}</div>
                        </div>
                        <div className="col-sm">
                            <label className="form-label">
                                <strong>Webiste</strong>
                            </label>
                            <div className="col-sm-12">{item.website}</div>
                        </div>
                        <div className="col-sm">
                            <label className="form-label">
                                <strong>Phone</strong>
                            </label>
                            <div className="col-sm-12">{item.phone}</div>
                        </div>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center mt-5">
                            <Link to={`/post/${item.id}`}>
                                <Button color='primary' size='sm'>View Post</Button>
                            </Link>
                            <Link to={`/albums/${item.id}`}>
                                <Button color='primary' size='sm'>View Album</Button>
                            </Link>
                        </div>
                    </div>
                </CardBody>
            </Card>
        </div>
    )
}

export default ItemRow

