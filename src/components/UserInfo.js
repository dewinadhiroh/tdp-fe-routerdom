import React from 'react'
import { Card, CardHeader, CardBody } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import BackButton from './BackButton';

const UserInfo = ({ item }) => {
    return (
        <div className='mb-5 mt-5'>
            <BackButton />
                    <div className='text-center'>
                        <h5>User Information</h5>
                    </div>
                    <Card className='mt-3'>
                        <CardHeader>{item.name}</CardHeader>
                        <CardBody>
                            <div className='row'>
                                <div className="col-sm">
                                    <label className="form-label">
                                        <strong>Username</strong>
                                    </label>
                                    <div className="col-sm-12">{item.username}</div>
                                </div>
                                <div className="col-sm">
                                    <label className="form-label">
                                        <strong>Email</strong>
                                    </label>
                                    <div className="col-sm-12">{item.email}</div>
                                </div>
                                <div className="col-sm">
                                    <label className="form-label">
                                        <strong>Webiste</strong>
                                    </label>
                                    <div className="col-sm-12">{item.website}</div>
                                </div>
                                <div className="col-sm">
                                    <label className="form-label">
                                        <strong>Phone</strong>
                                    </label>
                                    <div className="col-sm-12">{item.phone}</div>
                                </div>
                            </div>
                            <div className='row mt-3'>
                                <div className="col-sm">
                                    <label className="form-label">
                                        <strong>Address</strong>
                                    </label>
                                    <div className="col-sm-12">
                                    {item.address?.['suite']} {item.address?.['street']} {item.address?.['city']} {item.address?.['zipcode']}
                                    </div>
                                </div>
                                <div className="col-sm">
                                    <label className="form-label">
                                        <strong>Company</strong>
                                    </label>
                                    <div className="col-sm-12">
                                    {item.company?.['name']} {item.company?.['catchPhrase']} {item.company?.['bs']}
                                    </div>
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                </div>

    )
}
export default UserInfo