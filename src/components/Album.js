import React from 'react'
import { Card, CardBody, CardTitle, CardImg } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Album = ({item}) => {
    return (
            <div className='col-sm-4 mt-3'>
                <Card>
                    <CardImg alt="Card image cap" src="https://picsum.photos/318/180" top width="100%" />
                    <CardBody>
                        <CardTitle tag="h5">
                            {item.title}
                        </CardTitle>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center mt-4">
                            <a href={`/albums/${item.userId}/photos/${item.id}`} style={{ textDecoration: 'none' }}>
                                View Photos
                            </a>
                        </div>
                    </CardBody>
                </Card>
            </div>
    )
}

export default Album