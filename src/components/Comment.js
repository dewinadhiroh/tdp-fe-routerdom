import React from 'react'
import { Card, CardBody, CardTitle, CardText, CardSubtitle } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Comment = ({ item }) => {
    return (
            <div >
                <Card className='mt-3'>
                    <CardBody>
                        <CardTitle tag="h5">{item.name}</CardTitle>
                        <CardSubtitle className="mb-2 text-muted" tag="h6">
                            {item.email}
                        </CardSubtitle>
                        <CardText>
                            {item.body}
                        </CardText>
                    </CardBody>
                </Card>
            </div>
    )
}
export default Comment