import React from 'react'

const Title = () => {
    return (
        <div className='mt-5 text-center'>
            <h2>JSONPlaceholder</h2>
        </div>
    )
}
export default Title