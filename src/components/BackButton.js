import React from 'react'
import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from 'react-router-dom'

const BackButton = () => {
    const navigate = useNavigate();
  return (
    <div>
        <Button onClick={() => navigate(-1)} color="secondary">Back</Button>
    </div>
  )
}
export default BackButton