import React from 'react'
import { Card, CardBody, CardTitle, CardText, } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css'

const Post = ({ item }) => {
  return (
    <div>
      <Card className='mt-3'>
        <CardBody>
          <CardTitle tag="h5">{item.title}</CardTitle>
          <CardText>{item.body}</CardText>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center mt-2">
            <a href={`/post/${item.userId}/comments/${item.id}`} style={{ textDecoration: 'none' }}>
              View Comments
            </a>
          </div>
        </CardBody>
      </Card>
    </div>
  )
}
export default Post