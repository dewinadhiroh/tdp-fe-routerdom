import React from 'react'
import { Card, CardBody, CardTitle, CardImg } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Photo = ({ item }) => {
    return (
        <div className='col-sm-4 mt-3'>
            <Card>
                <CardImg alt="Card image cap" src={item.thumbnailUrl} top width="100%" />
                <CardBody>
                    <CardTitle tag="h5">
                        {item.title}
                    </CardTitle>
                </CardBody>
            </Card>
        </div>
    )
}
export default Photo