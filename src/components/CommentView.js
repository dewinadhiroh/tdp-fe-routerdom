import React from 'react'
import { Card, CardBody, CardText, CardHeader, } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import BackButton from '../components/BackButton'

const CommentView = ({ item }) => {
  return (
    <div className='mb-5'>
      <BackButton />
      <div className='text-center mb-5'>
        <h5>Post Detail</h5>
      </div>
      <Card className='mt-3'>
        <CardHeader>{item.title}</CardHeader>
        <CardBody>
          <CardText>{item.body}</CardText>
        </CardBody>
      </Card>
    </div>
  )
}
export default CommentView